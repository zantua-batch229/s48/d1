//console.log("Asdfg")

// dummy database

let posts = [];
let count =1;


// Add post data

document.querySelector('#form-add-post').addEventListener('submit', (e)=> {

	// this will prevent our webpage from reloading
	// the webpage reloads because of 'submit' event
	e.preventDefault();

	posts.push({
		id : count,
		title : document.querySelector('#txt-title').value,
		body : document.querySelector('#txt-body').value,
	})
	count++;

	showPosts(posts);
	alert("Successfully Added");
});

// show post

const showPosts = (posts) => {
	let postEntries = '';
	posts.forEach((post) => {
		postEntries += `

		<div id="post-${post.id}">
		
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>

		</div>

		`; 
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;

}

// edit post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#text-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

}

// delete post

const deletePost = (id) => {
	posts = posts.filter((post) => {
		if(post.id.toString() !== id) {
			return post;
		}
	})

	document.querySelector(`#post-${id}`).remove();
}

// update post
/*document.querySelector('#form-edit-post').addEventListener('submit', (e)=>{
	e.preventDefault();

	for(let i = 0; i < posts.length; i++){
		// the value post[].id is a number while document.querySelector
		//therefore, it is necessary to convertthe number to string

		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert("Updated successfully");

			break;
		}

	}

});*/

// Update Post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	for(let i = 0; i < posts.length; i++){
		// The value post[i].id is a Number while document.querySelector('#txt-edit-id').value is a String
		// Therefore, it is necessary to convert the Number to String

		if(posts[i].id.toString() === document.querySelector('#text-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert("Successfully Updated!");

			break;
		}
	}
})
